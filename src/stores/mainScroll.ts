import { atom } from "nanostores";

export const hasScrolledBottom = atom(false);
