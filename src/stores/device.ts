import { atom, onMount } from "nanostores";

export const isMobile = atom(false);

onMount(isMobile, () => {
  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    isMobile.set(true);
  } else {
    isMobile.set(false);
  }
});
