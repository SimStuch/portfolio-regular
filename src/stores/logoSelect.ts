import { atom, onMount } from "nanostores";

export const selectedLogo = atom<string | null>(null);

onMount(selectedLogo, (v) => {
  document.addEventListener("click", () => {
    selectedLogo.set(null);
  });
});
