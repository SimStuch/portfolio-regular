import * as Tooltip from "@radix-ui/react-tooltip";
import React, { useEffect, useMemo } from "react";
import { useState } from "react";
import { selectedLogo } from "../stores/logoSelect";
import { isMobile } from "../stores/device";

type Props = {
  children?: React.ReactNode;
  text: string;
  link?: string;
  id: string;
};

export const PopLogo = ({ children, text, link, id }: Props) => {
  const [selected, select] = useState<string | null>(null);
  useEffect(() => {
    const unsubscribe = selectedLogo.subscribe((value) => {
      select(value);
    });
    return () => {
      unsubscribe();
    };
  }, []);

  const [isMobileDevice, setIsMobileDevice] = useState(false);
  useEffect(() => {
    const unsubscribe = isMobile.subscribe((value) => {
      setIsMobileDevice(value);
    });
    return () => {
      unsubscribe();
    };
  }, []);

  const props = useMemo(() => {
    return isMobileDevice
      ? {
          open: selected == id,
        }
      : {};
  }, [isMobileDevice, selected]);

  return (
    <Tooltip.Provider delayDuration={50}>
      <Tooltip.Root {...props}>
        <Tooltip.Trigger
          onClick={(e) => {
            e.stopPropagation();
            if (selected == id) {
              select(null);
              selectedLogo.set(null);
              return;
            }
            select(id);
            selectedLogo.set(id);
          }}
          style={{
            padding: 0,
            border: "none",
            backgroundColor: "transparent",
            lineHeight: 0,
          }}
        >
          {children}
        </Tooltip.Trigger>
        <Tooltip.Portal>
          <Tooltip.Content className="TooltipContent" sideOffset={5}>
            <div
              style={{
                backgroundColor: "var(--primary)",
                opacity: 0.9,
                color: "white",
                padding: "5px 10px",
                borderRadius: 5,
              }}
            >
              <a
                href={link}
                target="_blank"
                style={{
                  color: "white",
                  textDecoration: "none",
                }}
              >
                {text}
              </a>
            </div>
          </Tooltip.Content>
        </Tooltip.Portal>
      </Tooltip.Root>
    </Tooltip.Provider>
  );
};
