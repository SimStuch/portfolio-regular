import { useCallback, useEffect, useState } from "react";
import Particles, { initParticlesEngine } from "@tsparticles/react";
import { loadFull } from "tsparticles";

const ParticlesBg = () => {
  const [init, setInit] = useState(false);

  useEffect(() => {
    if (!window) return;
    initParticlesEngine(async (engine) => {
      await loadFull(engine);
    }).then(() => {
      setInit(true);
    });
  }, []);

  const particlesLoaded = useCallback(async (container: any) => {
    console.log("Particles loaded", container);
  }, []);

  if (!init) return null;

  return (
    <Particles
      id="particles"
      particlesLoaded={particlesLoaded}
      options={{
        fpsLimit: 60,
        fullScreen: {
          enable: false,
          zIndex: -1,
        },
        interactivity: {
          detectsOn: "window",
          events: {
            resize: {
              enable: true,
            },
          },
        },
        particles: {
          color: {
            value: "#000000",
            animation: {
              enable: false,
            },
          },
          links: {
            color: "#000000",
            distance: 200,
            enable: true,
            opacity: 1,
            width: 1,
            triangles: {
              enable: true,
              color: "#ffff6a",
              opacity: 0.3,
            },
          },
          move: {
            direction: "none",
            enable: true,
            outModes: "bounce",
            random: false,
            speed: 5,
            straight: true,
            attract: {
              enable: true,
              distance: 200,
              rotate: {
                x: 600,
                y: 1200,
              },
            },
          },
          number: {
            density: {
              enable: true,
              height: 500,
              width: 500,
            },
            value: 3,
          },
          opacity: {
            value: 0,
          },
          shape: {
            close: true,
            options: {},
            type: "circle",
          },
          size: {
            animation: {
              mode: "auto",
            },
            value: 6,
          },
        },
        detectRetina: true,
      }}
    />
  );
};

export default ParticlesBg;
