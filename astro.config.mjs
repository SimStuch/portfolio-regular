import { defineConfig } from "astro/config";
import react from "@astrojs/react";
import sitemap from "@astrojs/sitemap";
import vercel from "@astrojs/vercel/static";

import mdx from "@astrojs/mdx";

// https://astro.build/config
export default defineConfig({
  site: "https://simonsauzede.fr",
  integrations: [
    react(),
    sitemap({}),
    mdx({
      customComponentNames: ["layout"],
    }),
  ],
  adapter: vercel(),
});
